<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <?php print $picture ?>
  <?php if ($page == 0): ?>
    <div class="date">
		<?php if ($submitted): ?>
		<span class="submitted"><?php print t('Posted ') . format_date($node->created, 'custom', "F jS, Y") . t(' by ') . theme('username', $node); ?></span>
	  	<?php endif; ?>
	</div>
  <h1 class="title"><a href="<?php print $node_url ?>"><?php print $title ?></a></h1>
  <?php endif; ?>
				<div class="content">
					<?php print $content?>
				</div>
</div>
				<div class="footer">
						<?php print $links?>
				</div>