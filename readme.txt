terrafirma for  DRUPAL 6
by nodethirtythree design
Terrafirma Theme is light-weight, 2-column, fixed width and uses CSS.

http://www.baruzh.com
http://www.nodethirtythree.com


This template is released under the Creative Commons Attributions 2.5 license, which
basically means you can do whatever you want with it provided you credit the author.
(ie. me). In the case of this template, a link back to my site is more than sufficient.

If you want to read the license, go here:

http://creativecommons.org/licenses/by/2.5/

