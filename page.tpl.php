<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
	terrafirma 1.0 by  nigraphic.com & nodethirtythree.com
-->
<html>
<head>
<?php print $head ?>
<title><?php print $head_title ?></title>
<meta name="keywords" content />
<meta name="description" content />
<?php print $styles?>
<?php print $scripts ?>
</head>
<body>
<div id="outer">
	<div id="upbg"></div>
	<div id="inner">
		<div id="header">
		      <div id="logo-title">
				<?php if ($logo): ?>
				  <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>">
					<img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" id="logo" />
				  </a>
				<?php endif; ?>
				</div>
			<div id="title">
				<h1><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print($site_name) ?></a></h1>
				<h2><?php print $site_slogan?></h2>
			</div>
		</div>
		<div id="splash"></div>
		<div id="menu">
        <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links) ?><?php } ?>
		<div id="date"><?php print date('l dS \of F Y');?></div>
		</div>
		<div id="primarycontent">
					<div class="post">
				<!-- start top block -->
				<div class="header">
					<?php print $header?>
				</div>
			</div>
		 <?php if ($messages != ""): ?>
          <?php print $messages ?>
        <?php endif; ?>
		 <?php if ($title != ""): ?>
          <?php print $breadcrumb ?>
          <h1 class="title"><?php print $title ?></h1>
          <?php if ($tabs != ""): ?>
            <div class="tabs"><?php print $tabs ?></div>
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($help != ""): ?>
            <div id="help"><?php print $help ?></div>
        <?php endif; ?>
			<!-- primary content start -->
			<div class="post">
			<?php print $content ?>
			</div>
		</div>
			<!-- primary content end -->
		</div>
		<?php if ($right): ?>
		<div id="secondarycontent">
			<!-- secondary content start -->
			<?php print $right?>              
			<!-- secondary content end -->
		</div>
		<?php endif; ?>
		<div id="footer">
			<a href="http://www.nigraphic.com">Design by niGraphic</a></div>
		</div>
<?php print $closure ?>  
</body>
</html>